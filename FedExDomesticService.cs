using AutoMapper;
using FedExShipments.WebUI.Exceptions;
using FedExShipments.WebUI.FedExDomesticWebService;
using FedExShipments.WebUI.Models;
using InspireCRM.WebUI.Helpers;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web;

namespace FedExShipments.WebUI.Services
{
    public class FedExDomesticService : IDisposable
    {
        private int _accessId;
        private string _accessCode;
        private static LogHelper _logger = new LogHelper();
        private bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public FedExDomesticService(int accessId, string accessCode)
        {
            _accessId = accessId;
            _accessCode = accessCode;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        /// <summary>
        /// Pobieranie obecnej wersji web serwisu FedEx
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetCurrentApiVersion()
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var response = await client.pobierzWersjeAsync(_accessCode);
                    return response.wersja;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania wersji API serwisu FedEx");
            }
        }

        /// <summary>
        /// Pobieranie dostępnych usług dla konta
        /// </summary>
        /// <param name="accountPostalCode">Kod pocztowy konta</param>
        /// <returns></returns>
        public async Task<IEnumerable<string[]>> GetAvailableServices(string accountPostalCode)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var availableServices = await client.pobierzDostepneUslugiAsync(_accessCode, accountPostalCode);

                    return availableServices.availableServices.Select(a => a.availableService);
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania dostępnych usług FedEx");
            }
        }

        /// <summary>
        /// Pobieranie statusów przesyłek
        /// </summary>
        /// <returns></returns>
        public async Task<List<FedExPackageStatusModel>> GetPackageStatuses(int limit)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var result = new List<FedExPackageStatusModel>();

                    var response = await client.pobierzZbiorStatusowAsync(_accessCode, limit.ToString());

                    var zbiory = response.pobierzZbiorStatusowResponse1;

                    foreach (var item in zbiory.parcelsStatuses)
                    {
                        result.Add(new FedExPackageStatusModel()
                        {
                            DeptSymbol = item.deptSymbol,
                            Description = item.description,
                            ParcelNumber = item.parcelNumber,
                            RecipientSignature = item.recipientSignature,
                            ShortStatus = item.shortStatus,
                            StatusDate = Convert.ToDateTime(item.statusDate)
                        });
                    }

                    return result;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania statusów przesyłek FedEx");
            }
        }
        
        /// <summary>
        /// Tworzenie przesyłki
        /// </summary>
        /// <param name="request">Model z danymi do przesyłki</param>
        /// <returns></returns>
        public async Task<FedExShipmentResponseModel> CreatePackage(FedExShipmentRequestModel request)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var result = new FedExShipmentResponseModel();

                    var listV2 = Mapper.Map<FedExShipmentRequestModel, listV2>(request);

                    var response = await client.zapiszListV2Async(_accessCode, listV2);

                    var przesylkaZapisanaV2 = response.przesylkaZapisanaV2;

                    if (przesylkaZapisanaV2 != null)
                    {
                        result = Mapper.Map<listZapisanyV2, FedExShipmentResponseModel>(przesylkaZapisanaV2);
                    }

                    return result;
                }
            }
            catch (FaultException ex)
            {
                throw new FedExException(ex.Message);
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new FedExException("Wystąpił błąd podczas tworzenia przesyłki FedEx");
            }
        }

        /// <summary>
        /// Pobieranie etykiety dla przesyłki (jednopaczkowej)
        /// </summary>
        /// <param name="shipmentNumber">Numer przesyłki</param>
        /// <returns>Etykieta</returns>
        public async Task<byte[]> GetShipmentLabel(string shipmentNumber)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var response = await client.wydrukujEtykieteAsync(_accessCode, shipmentNumber, "PDF");

                    var result = response.etykietaBajty;

                    return result;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania etykiety przesyłki FedEx");
            }
        }

        /// <summary>
        /// Pobieranie etykiety dla przesyłki (wielopaczkowej)
        /// </summary>
        /// <param name="parcelNumbers">Numery paczek w przesyłce</param>
        /// <returns>Kolekcja słownikowa [numer_przesyłki;etykieta]</returns>
        public async Task<Dictionary<string, byte[]>> GetShipmentLabel(string[] parcelNumbers)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var result = new Dictionary<string, byte[]>();
                    foreach (var parcelNumber in parcelNumbers)
                    {
                        var response = await client.wydrukujEtykietePaczkiAsync(_accessCode, parcelNumber, "PDF");

                        result.Add(parcelNumber, response.etykietaBajty);
                    }

                    return result;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania etykiet dla paczek w przesyłce FedEx");
            }
        }

        /// <summary>
        /// Generowanie dokumentu wydania
        /// </summary>
        /// <param name="shipmentNumbers">Numery przesyłek na wydaniu</param>
        /// <returns></returns>
        public async Task<byte[]> GenerateDispatchOrder(string[] shipmentNumbers, long courierId)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var response = await client.zapiszDokumentWydaniaAsync(_accessCode, string.Join(";", shipmentNumbers), ";", courierId);

                    return response.dokumentWydaniaPdf;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas generowania dokumentu zlecenia odbioru wybranych przesyłek FedEx");
            }
        }

        /// <summary>
        /// Pobieranie dokumentu/ów wydania
        /// KROK 1 - pobierzNumerDokumentuList
        /// KROK 2 - pobierzDokumentWydania
        /// </summary>
        /// <param name="shipmentNumbers">Numery przesyłek</param>
        /// <returns></returns>
        public async Task<Dictionary<string, byte[]>> GetDispatchOrderPDF(string[] shipmentNumbers)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var result = new Dictionary<string, byte[]>();

                    foreach (var shipmentNumber in shipmentNumbers)
                    {
                        var response = await client.pobierzNumerDokumentuListAsync(_accessCode, shipmentNumber);

                        byte[] label = null;

                        if (response.numerDokumentu != null)
                        {
                            var response2 = await client.pobierzDokumentWydaniaAsync(_accessCode, response.numerDokumentu);
                            label = response2.dokumentWydaniaPdf;
                        }

                        result.Add(response.numerDokumentu, label);
                    }

                    return result;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania dokumentów zlecenia odbioru przesyłek FedEx");
            }
        }

        public async Task<byte[]> GetDispatchOrderPDF(string dispatchNumber)
        {
            try
            {
                using (var client = new IklServiceClient())
                {
                    var response = await client.pobierzDokumentWydaniaAsync(_accessCode, dispatchNumber);

                    return response.dokumentWydaniaPdf;
                }
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLog(ex);
                throw new Exception("Wystąpił błąd podczas pobierania dokumentu zlecenia odbioru przesyłek FedEx");
            }
        }


        private void ExceptionLog(Exception ex)
        {
            Task.Factory.StartNew(() => SendTechEmailHelper.SendMessage(ex));
            _logger.Logger.Error(ex);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    handle.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
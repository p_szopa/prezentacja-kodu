using InspireCRM.Domain.Abstract;
using InspireCRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace InspireCRM.Domain.Concrete
{
    public class EFGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private IDbContext _context;
        private DbSet<TEntity> DbSet
        {
            get
            {
                return _context.Set<TEntity>();
            }
        }

        /// <summary>
        /// Konstruktor tworzący dostęp do bazy danych i inicjalizujący dane z wybranej encji
        /// </summary>
        /// <param name="context">Przekazane instancji bazy danych</param>
        public EFGenericRepository(IDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Metoda asynchroniczna pobierająca wszystkie dane z wybranej encji bazy danych
        /// </summary>
        /// <param name="filter">
        /// Parametr wykorzystywany do sprecyzowania pobieranych danych w formie wyrażenia lambda.
        /// Dla zadanego wyrażenia rezultatem będzie wartość Boolean
        /// Na przykład, gdy repozytorium jest instancją encji typu Products i mamy znaleźć produkt o nazwie Pomidor, wyrażenie lambda powinno przyjąć postać: product => product.ProductName == "Pomidor".
        /// </param>
        /// <param name="orderBy">
        /// Parametr ten służy do sortowania elementów według zadanego filtru poprzez wyrażenie lambda.
        /// Wyrażenie zwróci uporządkowane dane w formie obiektu IQueryable
        /// Na przykład, jeżeli repozytorium jest instancją encji typu Products i chcemy pobrać posortowane, elementy z kolumny ProductName: q => q.OrderBy(s => s.ProductName)
        /// </param>
        /// <param name="includeProperties">
        /// Parametr ten służy do przesłania innych encji do zapytania. Oddzielone są przecinkami, a całość wyrażenia jest typu string.
        /// </param>
        /// <returns>
        /// Wartością zwracaną jest kolekcja wyszukanych danych dla wskazanej encji
        /// </returns>
        public virtual async Task<IEnumerable<TEntity>> GetAsync(
           Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           int? skip = null,
           int? take = null,
           string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                if (skip.HasValue && take.HasValue)
                {
                    return await orderBy(query).Skip(skip.Value).Take(take.Value).ToListAsync();
                }
                else if (skip.HasValue && !take.HasValue)
                {
                    return await orderBy(query).Skip(skip.Value).ToListAsync();
                }
                else if (!skip.HasValue && take.HasValue)
                {
                    return await orderBy(query).Take(take.Value).ToListAsync();
                }
                else
                {
                    return await orderBy(query).ToListAsync();
                }
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        /// <summary>
        /// Metoda asynchroniczna pobierająca pierwszy rekord zapytania
        /// </summary>
        /// <param name="filter">Parametr wykorzystywany do sprecyzowania pobieranych danych w formie wyrażenia lambda</param>
        /// <param name="orderBy">Sortowanie</param>
        /// <param name="skip">Liczba pominietych rekordow</param>
        /// <returns>Pierwszy rekord dla wskazanej encji (lub wartosc null w przypadku braku wynikow)</returns>
        public virtual async Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           int? skip = null)
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                if (skip.HasValue)
                {
                    return await orderBy(query).Skip(skip.Value).FirstOrDefaultAsync<TEntity>();
                }
                else
                {
                    return await orderBy(query).FirstOrDefaultAsync<TEntity>();
                }
            }
            else
            {
                return await query.FirstOrDefaultAsync<TEntity>();
            }
        }

        /// <summary>
        /// Metoda asynchroniczna pobiera ostatni rekord podanej encji
        /// </summary>
        /// <param name="keySelector">Selektor po ktorym ma zostac wybrany ostatni rekord (zazwyczaj x=>x.Id)</param>
        /// <param name="filter">Parametr wykorzystywany do sprecyzowania pobieranych danych w formie wyrażenia lambda</param>
        /// <returns>Ostatni rekord dla wskazanej encji (lub wartosc null w przypadku braku wynikow)</returns>
        public virtual async Task<TEntity> GetLastOrDefaultAsync<TKey>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TKey>> keySelector)
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.OrderByDescending(keySelector).FirstOrDefaultAsync<TEntity>();
        }


        /// <summary>
        /// Metoda asynchroniczna pobierająca liczbę rekordów
        /// </summary>
        /// <param name="filter">Parametr wykorzystywany do sprecyzowania pobieranych danych w formie wyrażenia lambda</param>
        /// <returns>Liczba rekordów w danej encji</returns>
        public virtual async Task<int> GetCountAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            int queryCount = filter == null ? await DbSet.CountAsync() : await DbSet.CountAsync(filter);
            return queryCount;
        }

        /// <summary>
        /// Metoda asynchroniczna wyszukująca dane ze wskazanej encji poprzez wartość klucza głównego
        /// </summary>
        /// <param name="id">Wartość odpowiadająca kluczowi głównemu w wybranej encji</param>
        /// <returns>Zwraca dane z wiersza o zadanym ID</returns>
        public virtual async Task<TEntity> GetByIDAsync(object id)
        {
            return await DbSet.FindAsync(id);
        }

        /// <summary>
        /// Metoda wstawiająca dane do wskazanej encji
        /// </summary>
        /// <param name="entity">Parametr służący do przesłania wybranej encji</param>
        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        /// <summary>
        /// Metoda usuwająca rekord z wybranej encji
        /// </summary>
        /// <param name="id">Parametr służący do przesłania identyfikatora rekordu encji do skasowania</param>
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// Metoda usuwająca rekord z wybranej encji
        /// </summary>
        /// <param name="entityToDelete">Parametr służący do przesłania rekordu encji do skasowania</param>
        public virtual void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        /// <summary>
        /// Metoda aktualizująca rekord w wybranej encji
        /// </summary>
        /// <param name="entityToUpdate">Parametr służący do przesłania rekordu encji do zaktualizowania</param>
        public virtual void Update(TEntity entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}
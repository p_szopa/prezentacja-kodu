using AllegroOrders.WebUI.Enums;
using AllegroOrders.WebUI.Exceptions;
using AllegroOrders.WebUI.Models.AllegroRestApiModel;
using InspireCRM.WebUI.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AllegroOrders.WebUI.Helpers.AllegroRestApiHelpers
{
    public class AllegroRestApiService
    {
        readonly string _clientSecret;
        readonly string _clientId;
        readonly string _api;
        readonly string _authApi;

        private static LogHelper _logger;

        public AllegroRestApiHelper()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _api = ConfigurationManager.AppSettings["AllegroRestApiUrl"];
            _authApi = ConfigurationManager.AppSettings["AllegroOAuthUrl"];
            _clientId = ConfigurationManager.AppSettings["AllegroRestApiClientId"];
            _clientSecret = ConfigurationManager.AppSettings["AllegroRestApiClientSecret"];
            _logger = new LogHelper();
        }

        /// <summary>
        /// Metoda pobierająca identyfikator użytkownika (sprzedawcy) Allegro
        /// </summary>
        /// <param name="userCredentials"></param>
        /// <returns></returns>
        public async Task<string> GetUserId(UserCredentials userCredentials)
        {
            try
            {
                using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken))
                {
                    var uriMethod = string.Format("/me");

                    var response = await client.GetAsync(_api + uriMethod);

                    if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
                        response = await RefreshTokenAndRetryRequest(userCredentials, HttpVerbs.Get, uriMethod);

                    string contentResult = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError("GetUserId", response);
                        throw new AllegroException(contentResult);
                    }

                    var model = JsonConvert.DeserializeObject<BasicInformationUserModel>(contentResult);

                    return model != null ? model.Id : "";
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => SendTechEmailHelper.SendMessage(ex)).Wait();
                throw ex;
            }
        }

        /// <summary>
        /// Pobiera szczegóły zamówienia na podstawie przekazanego identyfikatora transakcji
        /// </summary>
        /// <param name="userCredentials">Poświadczenia konta Allegro</param>
        /// <param name="transactionIdentifier">Identyfikator transakcji</param>
        /// <returns></returns>
        public async Task<CheckoutForm> GetOrderDetails(UserCredentials userCredentials, Guid transactionIdentifier)
        {
            try
            {
                using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken))
                {
                    CheckoutForm result = new CheckoutForm();

                    var uriMethod = string.Format("/order/checkout-forms/{0}", transactionIdentifier);

                    var response = await client.GetAsync(_api + uriMethod);

                    if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
                        response = await RefreshTokenAndRetryRequest(userCredentials, HttpVerbs.Get, uriMethod);

                    string contentResult = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError("GetOrderDetails", response);
                        throw new AllegroException(contentResult);
                    }

                    return JsonConvert.DeserializeObject<CheckoutForm>(contentResult);
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => SendTechEmailHelper.SendMessage(ex)).Wait();
                throw ex;
            }
        }

        /// <summary>
        /// Metoda pobierająca dostępne metody dostawy
        /// </summary>
        /// <param name="userCredentials">Poświadczenia konta Allegro</param>
        /// <returns>Dostępne metody dostawy</returns>
        public async Task<DeliveryMethodsResponseModel> GetDeliveryMethods(UserCredentials userCredentials)
        {
            try
            {
                using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken))
                {
                    var uriMethod = "/sale/delivery-methods";

                    var response = await client.GetAsync(_api + uriMethod);

                    if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
                        response = await RefreshTokenAndRetryRequest(userCredentials, HttpVerbs.Get, uriMethod);

                    string contentResult = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError("GetDeliveryMethods", response);
                        throw new AllegroException(contentResult);
                    }
                    else
                    {
                        var deliveryMethods = JsonConvert.DeserializeObject<DeliveryMethodsResponseModel>(contentResult);
                        return deliveryMethods;
                    }
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => SendTechEmailHelper.SendMessage(ex)).Wait();
                throw ex;
            }
        }

        /// <summary>
        /// Metoda przekazująca numer przesyłki
        /// </summary>
        /// <param name="userCredentials">Poświadczenia konta Allegro</param>
        /// <param name="packageList">Lista przesyłek</param>
        /// <param name="allegroOrderId">Id zamówienia Allegro</param>
        /// <returns></returns>
        public async Task<PackageResponseModel> SetPackageNumber(UserCredentials userCredentials, PackageRequestModel packageList, Guid allegroOrderId)
        {
            try
            {
                using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken))
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(packageList), null, AllegroSchemaEnum.Public.GetDescription());
                    content.Headers.ContentType.CharSet = "";
                    var uriMethod = string.Format("/order/checkout-forms/{0}/shipments", allegroOrderId);
                    var contentSaved = await content.ReadAsStringAsync();

                    var response = await client.PostAsync(_api + uriMethod, content);

                    if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
                        response = await RefreshTokenAndRetryRequest(userCredentials, HttpVerbs.Post, uriMethod);

                    string contentResult = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError("SetPackageNumber", response, contentSaved);
                        throw new AllegroException(contentResult);
                    }
                    else
                    {
                        if (contentResult.Contains("\"code\":\"ERROR\""))
                        {
                            await SendTechEmailHelper.SendMessage("Bład podczas wysyłania numeru przesyłki", contentResult);
                            LogError("SetPackageNumber", response, contentSaved);
                            throw new AllegroException(contentResult);
                        }
                        else
                        {
                            var packagesReponse = JsonConvert.DeserializeObject<PackageResponseModel>(contentResult);
                            return packagesReponse;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => SendTechEmailHelper.SendMessage(ex)).Wait();
                throw ex;
            }
        }

        /// <summary>
        /// Metoda inicjalizująca wiązanie konta Allegro z systemem - pierwszy etap
        /// </summary>
        /// <returns>Adres do weryfikacji użytkownika</returns>
        public async Task<InitAccountBindResponseModel> InitAccountBind()
        {
            try
            {
                using (var client = HttpClientHelper.GetClient(_clientId, _clientSecret))
                {
                    StringContent content = new StringContent(string.Format("client_id={0}", _clientId), Encoding.UTF8, "application/x-www-form-urlencoded");
                    var response = await client.PostAsync(_authApi + "/device", content);
                    string contentResult = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError("InitAccountBind", response);
                        throw new AllegroException(contentResult);
                    }
                    else
                    {
                        var initAccountBindResponse = JsonConvert.DeserializeObject<InitAccountBindResponseModel>(contentResult);
                        return initAccountBindResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => SendTechEmailHelper.SendMessage(ex)).Wait();
                throw ex;
            }
        }


        /// <summary>
        /// Metoda odwieżająca poświadczenia użytkownika
        /// </summary>
        /// <param name="userCredentials"></param>
        /// <returns></returns>
        private async Task RefreshUserCredentials(UserCredentials userCredentials)
        {
            try
            {
                using (var client = HttpClientHelper.GetClient(_clientId, _clientSecret))
                {
                    var response = await client.PostAsync(_authApi + string.Format("/token?grant_type=refresh_token&refresh_token={0}", userCredentials.RefreshToken), null);
                    string contentResult = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        LogError("RefreshUserCredentials", response);
                        throw new AllegroBindResultException(contentResult);
                    }
                    else
                    {
                        var accessTokenResponse = JsonConvert.DeserializeObject<AccessTokenResponseModel>(contentResult);
                        userCredentials.SetNewUserCredentials(accessTokenResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => SendTechEmailHelper.SendMessage(ex)).Wait();
                throw ex;
            }
        }

        /// <summary>
        /// Metoda odświeżająca token oraz ponawiająca żądanie
        /// </summary>
        /// <param name="httpMethod">Typ metody HTTP</param>
        /// <param name="uriMethod">URI metody żądania</param>
        /// <param name="content">Body dla żądania (dla metod HTTP GET i DELETE domyślnie null)</param>
        /// <param name="schema">Wersja metody</param>
        /// <returns>Response</returns>
        private async Task<HttpResponseMessage> RefreshTokenAndRetryRequest(UserCredentials userCredentials, HttpVerbs httpMethod, string uriMethod, HttpContent content = null, AllegroSchemaEnum schema = AllegroSchemaEnum.Public)
        {
            await RefreshUserCredentials(userCredentials);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.MethodNotAllowed);
            switch (httpMethod)
            {
                case HttpVerbs.Delete:
                    using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken))
                    {
                        response = await client.DeleteAsync(_api + uriMethod);
                    }
                    break;
                case HttpVerbs.Get:
                    using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken, schema))
                    {
                        response = await client.GetAsync(_api + uriMethod);
                    }
                    break;
                case HttpVerbs.Post:
                    using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken, schema))
                    {
                        response = await client.PostAsync(_api + uriMethod, content);
                    }
                    break;
                case HttpVerbs.Put:
                    using (var client = HttpClientHelper.GetClient(userCredentials.AccessToken, schema))
                    {
                        response = await client.PutAsync(_api + uriMethod, content);
                    }
                    break;
                default:
                    break;
            }

            return response;
        }

        /// <summary>
        /// Metoda pomocniczna przygotowująca log błędu
        /// </summary>
        /// <param name="actionName">Nazwa metody</param>
        /// <param name="httpMessage">Wiadomość http</param>
        /// <param name="requestContent">Zawartość request'a - tylko dla wiadomości typu POST</param>
        private void LogError(string actionName, HttpResponseMessage httpMessage, string requestContent = null)
        {
            _logger.Logger.Error("AllegroError - " + actionName + " || Request: " + httpMessage.RequestMessage.ToString() + " " + requestContent + " || Response: " + httpMessage.Content.ReadAsStringAsync().Result);
        }
    }
}